<%@ taglib prefix="c" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: mad
  Date: 19.12.16
  Time: 12:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Выписка</title>
    <link href="jquery/jquery-ui.css" rel="stylesheet">
    <script src="jquery/external/jquery/jquery.js">

    </script>
    <script src="jquery/jquery-ui.js">

    </script>

</head>
<body>
    <h1>Выписка</h1>
<c:form modelAttribute="form27y" method="post" action="/editform">
    <c:hidden path="id"/>
    <c:select path="personId" disabled="${!isadd}" onchange="changePerson(this)">
        <c:options items="${persons}" itemValue="id" itemLabel="fio"/>
    </c:select>
    <br>
    <label>День рождения:</label><span id="birthday">${form27y.birthday}</span>
    <br>
    <label>Адрес:</label><span id="address">${form27y.address}</span>
    <br>
    <label>Место работы:</label><span id="job">${form27y.job}</span>
    <br>
    <label>Направление в </label><c:input path="directionToHospital"/>
    <br>
    <label>Заблоевание:</label><c:input path="disease"/>
    <br>
    <label>Направляется с:</label>
    <c:input path="income"/>
    по
    <c:input path="outcome"/>
    <br>
    <label>Диагноз:</label>
    <c:input path="diagnosis"/>
    <br>
    <label>Анамнез:</label>
    <c:input path="anamnesis"/>
    <br>
    <label>Рекомендации:</label>
    <c:input path="recommendations"/>
    <br>
    <label>Дата документа:</label>
    <c:input path="dateDocument"/>
    <br>
    <label>Врач:</label><span id="therapist">${therapist.fio}</span>
    <hr>
    <input type="submit" value="ВВОД"/>
    <a href="/"><input type="button" value="ОМЕНА"/></a>
    <core:if test="${!isadd}">
        <a href="/delete?form27yId=${form27y.id}"><input type="button" value="УДАЛИТЬ" ></a>
    </core:if>
</c:form>

</body>
<script>
    $(function() {
       $('#dateDocument').datepicker({
           dateFormat: 'yy-mm-dd'
       });
        $('#income').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#outcome').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        setPerson(1);
    });

    function changePerson(select) {
        var id = $(select).val();
        setPerson(id);
    }

    function setPerson(id) {
        var url = "/person/"+id;
        $.getJSON(url, function(data){
            $('#birthday').html(data.birthday);
            $('#address').html(data.address);
            $('#job').html(data.job);
        });
    }
</script>
</html>
