<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Это тест</title>
    <script src="/js/app.js">

    </script>
</head>
<body>

<h1>Врач: ${therapist.fio}</h1>
<form method="post" action="/logout">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input type="submit" value="выход">
</form>
<hr>
<a href="/addForm"><button>Добавить выписку</button></a>
<table border="1">
    <tr>
        <th>ФИО</th>
        <th>Дата выписки</th>
        <th>Прибытие</th>
        <th>Убытие</th>
        <th>Действия</th>
    </tr>
    <c:forEach items="${form27y}" var="item">
        <tr>
            <td>${item.person.fio}</td>
            <td>${item.dateDocument}</td>
            <td>${item.income}</td>
            <td>${item.outcome}</td>
            <td>
                <a href="/editform?form27yId=${item.id}"><button>Изменить</button></a>
                <a href="/delete?form27yId=${item.id}"><button>Удалить</button></a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>