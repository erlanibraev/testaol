package kz.astonline.test.web.services.impl;

import kz.astonline.test.dao.repository.IForm27yRepository;
import kz.astonline.test.dao.repository.IPersonRepository;
import kz.astonline.test.dao.repository.ITherapistRepository;
import kz.astonline.test.dao.repository.entities.Form27yEntity;
import kz.astonline.test.dao.repository.entities.PersonEntity;
import kz.astonline.test.dao.repository.entities.TherapistEntity;
import kz.astonline.test.web.model.Form27yModel;
import kz.astonline.test.web.services.IForm27yService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

/**
 * Создал Ибраев Ерлан 19.12.16.
 */
@Service
public class Form27yService implements IForm27yService {
    private IForm27yRepository form27yRepository;
    private ITherapistRepository therapistRepository;
    private IPersonRepository personRepository;

    @Override
    public List<Form27yEntity> getByTherapist(String userName) {
        TherapistEntity therapist = therapistRepository.findOneByUserName(userName);
        return form27yRepository.findByTherapist(therapist);
    }

    @Override
    public void edit(Form27yModel form27y) {
        Form27yEntity entity = form27y.getId() != null ? form27yRepository.findOne(form27y.getId()): new Form27yEntity();
        PersonEntity person = personRepository.findOne(form27y.getPersonId());
        TherapistEntity therapist = therapistRepository.findOne(form27y.getTherapistId());
        entity.setPerson(person);
        entity.setTherapist(therapist);
        entity.setAnamnesis(form27y.getAnamnesis());
        entity.setDiagnosis(form27y.getDiagnosis());
        entity.setDirectionToHospital(form27y.getDirectionToHospital());
        entity.setDisease(form27y.getDisease());
        entity.setHospital(form27y.getHospital());
        entity.setRecommendations(form27y.getRecommendations());
        entity.setIncome(form27y.getIncome());
        entity.setOutcome(form27y.getOutcome());
        entity.setDateDocument(form27y.getDateDocument());
        form27yRepository.save(entity);
    }

    @Override
    public Form27yModel getForm(Long form27yId) {
        Form27yEntity entity = form27yRepository.findOne(form27yId);
        Form27yModel result = new Form27yModel();
        result.setDateDocument(new Date(new java.util.Date().getTime()));
        if (entity != null) {
            result.setId(entity.getId());
            result.setPersonId(entity.getPerson().getId());
            result.setTherapistId(entity.getTherapist().getId());
            result.setIncome(entity.getIncome());
            result.setOutcome(entity.getOutcome());
            result.setDateDocument(entity.getDateDocument());
            result.setRecommendations(entity.getRecommendations());
            result.setAnamnesis(entity.getAnamnesis());
            result.setDiagnosis(entity.getDiagnosis());
            result.setDirectionToHospital(entity.getDirectionToHospital());
            result.setDisease(entity.getDisease());
            result.setHospital(entity.getHospital());
            if(entity.getPerson() != null) {
                result.setBirthday(entity.getPerson().getBirthday());
                result.setAddress(entity.getPerson().getAddress());
                result.setJob(entity.getPerson().getJob());
            }
        }
        return result;
    }

    @Override
    public void delete(Long form27yId) {
        form27yRepository.delete(form27yId);
    }

    @Autowired
    public void setForm27yRepository(IForm27yRepository form27yRepository) {
        this.form27yRepository = form27yRepository;
    }

    @Autowired
    public void setTherapistRepository(ITherapistRepository therapistRepository) {
        this.therapistRepository = therapistRepository;
    }

    @Autowired
    public void setPersonRepository(IPersonRepository personRepository) {
        this.personRepository = personRepository;
    }
}
