package kz.astonline.test.web.controllers;

import kz.astonline.test.dao.repository.IPersonRepository;
import kz.astonline.test.dao.repository.ITherapistRepository;
import kz.astonline.test.dao.repository.entities.PersonEntity;
import kz.astonline.test.dao.repository.entities.TherapistEntity;
import kz.astonline.test.web.model.Form27yModel;
import kz.astonline.test.web.services.IForm27yService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;

/**
 * Created by mad on 18.12.2016.
 */
@Controller
public class IndexController {

    private IPersonRepository personRepository;
    private IForm27yService form27yService;
    private ITherapistRepository therapistRepository;

    @RequestMapping(value=""
            , method = RequestMethod.GET
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String index(Model model) {
        model.addAttribute("form27y", form27yService.getByTherapist(getUserName()));
        model.addAttribute("therapist", therapistRepository.findOneByUserName(getUserName()));
        return "index";
    }

    @RequestMapping(value="/addForm", method = RequestMethod.GET
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String addForm(Model model) {
        TherapistEntity therapist = therapistRepository.findOneByUserName(getUserName());
        model.addAttribute("therapist", therapist);
        model.addAttribute("persons", personRepository.findAll());
        model.addAttribute("isadd",true);
        return "editform";
    }

    @RequestMapping(value="/editform", method = RequestMethod.GET
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String editForm(@RequestParam("form27yId")Long form27yId, Model model) {
        TherapistEntity therapist = therapistRepository.findOneByUserName(getUserName());
        Form27yModel form27y = form27yService.getForm(form27yId);
        model.addAttribute("form27y", form27y);
        model.addAttribute("therapist", therapist);
        model.addAttribute("persons", personRepository.findAll());
        model.addAttribute("isadd",false);
        return "editform";
    }

    @RequestMapping(value="/delete", method = RequestMethod.GET
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String delete(@RequestParam("form27yId")Long form27yId, Model model) {
        form27yService.delete(form27yId);
        return "redirect:/";
    }

    @RequestMapping(value="/editform", method = RequestMethod.POST
            , produces = MediaType.TEXT_HTML_VALUE + ";charset=utf-8")
    public String editForm(@ModelAttribute("form27y")Form27yModel form27y) {
        TherapistEntity therapist = therapistRepository.findOneByUserName(getUserName());
        form27y.setTherapistId(therapist != null ? therapist.getId() : null);
        form27yService.edit(form27y);
        return "redirect:/";
    }

    @RequestMapping(value="/person/{id}"
            , method = RequestMethod.GET
            , produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public PersonEntity getPerson(@PathVariable("id") Long id) {
        return personRepository.findOne(id);
    }

    @ModelAttribute(name = "form27y")
    public Form27yModel getForm27yModel() {
        Form27yModel result = new Form27yModel();
        result.setDateDocument(new Date(new java.util.Date().getTime()));
        return result;
    }

    @Autowired
    public void setPersonRepository(IPersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Autowired
    public void setForm27yService(IForm27yService form27yService) {
        this.form27yService = form27yService;
    }

    @Autowired
    public void setTherapistRepository(ITherapistRepository therapistRepository) {
        this.therapistRepository = therapistRepository;
    }

    private String getUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        return name;
    }
}
