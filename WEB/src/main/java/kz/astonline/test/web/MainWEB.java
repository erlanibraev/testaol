package kz.astonline.test.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.reflect.Field;
import java.nio.charset.Charset;

/**
 * Created by mad on 18.12.2016.
 */
@SpringBootApplication
public class MainWEB {
    public static void main(String... args) throws NoSuchFieldException, IllegalAccessException {
        System.setProperty("file.encoding","UTF-8");
        Field charset = Charset.class.getDeclaredField("defaultCharset");
        charset.setAccessible(true);
        charset.set(null,null);
        SpringApplication.run(MainWEB.class, args);
    }
}
