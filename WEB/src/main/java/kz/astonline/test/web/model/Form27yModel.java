package kz.astonline.test.web.model;

import java.sql.Date;

/**
 * Создал Ибраев Ерлан 19.12.16.
 */
public class Form27yModel {

    private Long id;
    private Long personId;
    private Date birthday;
    private String address;
    private String job;
    private String disease;
    private String directionToHospital;
    private String hospital;
    private Date income;
    private Date outcome;
    private String diagnosis;
    private String anamnesis;
    private String recommendations;
    private Date dateDocument;
    private Long therapistId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getDirectionToHospital() {
        return directionToHospital;
    }

    public void setDirectionToHospital(String directionToHospital) {
        this.directionToHospital = directionToHospital;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public Date getIncome() {
        return income;
    }

    public void setIncome(Date income) {
        this.income = income;
    }

    public Date getOutcome() {
        return outcome;
    }

    public void setOutcome(Date outcome) {
        this.outcome = outcome;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    public Date getDateDocument() {
        return dateDocument;
    }

    public void setDateDocument(Date dateDocument) {
        this.dateDocument = dateDocument;
    }

    public Long getTherapistId() {
        return therapistId;
    }

    public void setTherapistId(Long therapistId) {
        this.therapistId = therapistId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
