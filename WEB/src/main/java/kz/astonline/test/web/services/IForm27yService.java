package kz.astonline.test.web.services;

import kz.astonline.test.dao.repository.entities.Form27yEntity;
import kz.astonline.test.web.model.Form27yModel;

import java.util.List;

/**
 * Создал Ибраев Ерлан 19.12.16.
 */
public interface IForm27yService {

    public List<Form27yEntity> getByTherapist(String userName);

    public void edit(Form27yModel form27y);

    public Form27yModel getForm(Long form27yId);

    public void delete(Long form27yId);
}
