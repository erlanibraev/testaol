Тестовое задание ТОО "AstOnLine"

Модули:
1) DAO - работа с БД
Используется БД H2.
Схема в файле DAO/src/main/resources/schema.sql;
Тестовые данные в файле DAO/src/main/resources/import.sql;

2) WEB - веб-интерфейс
При сборке создаются два артефакта в WEB/target

WEB-1.0-SNAPSHOT.war - для деплоя на сервер приложений
WEB-1.0-SNAPSHOT-exec.war - исполняемый файл
Запуск:
java -jar WEB-1.0-SNAPSHOT-exec.war
Веб интерфейс будет доступен по адресу: http://localhost:8080

Пользователь    пароль
test1           test1
test2           test2
test3           test3

проект: https://erlanibraev@bitbucket.org/erlanibraev/testaol.git