insert into users (id, fio, username, password, enabled) values (1, 'Иванов Иван Иванович', 'test1', 'test1', true);
insert into users (id, fio, username, password, enabled) values (2, 'Петров Петр Петрович', 'test2', 'test2', true);
insert into users (id, fio, username, password, enabled) values (3, 'Сидоров Сидор Сидорович', 'test3', 'test3', true);

insert into authorities (id, username, authority) values (1, 'test1', 'ADMIN');
insert into authorities (id, username, authority) values (2, 'test2', 'ADMIN');
insert into authorities (id, username, authority) values (3, 'test3', 'ADMIN');
insert into authorities (id, username, authority) values (4, 'test1', 'USER');
insert into authorities (id, username, authority) values (5, 'test2', 'USER');
insert into authorities (id, username, authority) values (6, 'test3', 'USER');


insert into person (id, fio, birthday, address, job) values (1, 'Сколов Иван Константинович', '1951-01-23', 'ул. Улица, дом 1, кв 33', 'нет');
insert into person (id, fio, birthday, address, job) values (2, 'Полуэктов Яно Полуэктович', '1965-03-13', 'ул. Улица, дом 4, кв 11', 'нет');
insert into person (id, fio, birthday, address, job) values (3, 'Коротков Семен Семенович', '2000-02-12', 'ул. Улица, дом 34, кв 250', 'нет');

insert into form27y (id, person, disease, directiontohospital, hospital, income, outcome, diagnosis, anamnesis, recommendations, datedocument, therapist) values (1, 1, 'TEST', 'TEST', 'TEST', '2016-01-01', '2016-01-31', 'TEST', 'TEST', 'TEST', '2016-02-01', 1);