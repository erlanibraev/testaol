package kz.astonline.test.dao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by mad on 18.12.2016.
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = {"kz.astonline.test.dao.repository"})
public class MainDAO {

    public static void main(String... args) {
        SpringApplication.run(MainDAO.class, args);
    }
}
