package kz.astonline.test.dao.repository.entities;

import javax.persistence.*;

/**
 * Создал Ибраев Ерлан 19.12.16.
 */
@Entity
@Table(name="authorities")
public class AuthoritiesEntity {

    private Long id;
    private String userName;
    private String authority;

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="username")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name="authority")
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
