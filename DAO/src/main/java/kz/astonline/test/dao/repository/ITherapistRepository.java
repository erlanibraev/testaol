package kz.astonline.test.dao.repository;

import kz.astonline.test.dao.repository.entities.TherapistEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by mad on 18.12.2016.
 */
@Repository
public interface ITherapistRepository extends JpaRepository<TherapistEntity, Long>, JpaSpecificationExecutor<TherapistEntity> {

    public TherapistEntity findOneByUserName(@Param("userName") String userName);
}
