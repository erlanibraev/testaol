package kz.astonline.test.dao.repository.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by mad on 18.12.2016.
 */
@Entity
@Table(name="users")
public class TherapistEntity {
    private Long id;
    private String fio;
    private String userName;
    private String password;
    private boolean enabled = true;
    private Set<Form27yEntity> form27yEntities;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="fio")
    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    @Column(name="username")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Column(name="password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name="enabled")
    public boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "therapist", cascade = CascadeType.ALL)
    @JsonIgnore
    public Set<Form27yEntity> getForm27yEntities() {
        return form27yEntities;
    }

    public void setForm27yEntities(Set<Form27yEntity> form27yEntities) {
        this.form27yEntities = form27yEntities;
    }
}
