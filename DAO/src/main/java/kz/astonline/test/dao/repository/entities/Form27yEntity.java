package kz.astonline.test.dao.repository.entities;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by mad on 18.12.2016.
 */
@Entity
@Table(name="form27y")
public class Form27yEntity {
    private Long id;
    private PersonEntity person;
    private String disease;
    private String directionToHospital;
    private String hospital;
    private Date income;
    private Date outcome;
    private String diagnosis;
    private String anamnesis;
    private String recommendations;
    private Date dateDocument;
    private TherapistEntity therapist;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="person")
    public PersonEntity getPerson() {
        return person;
    }

    public void setPerson(PersonEntity person) {
        this.person = person;
    }

    @Column(name="disease")
    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    @Column(name="directiontohospital")
    public String getDirectionToHospital() {
        return directionToHospital;
    }

    public void setDirectionToHospital(String directionToHospital) {
        this.directionToHospital = directionToHospital;
    }

    @Column(name="hospital")
    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    @Column(name="income")
    public Date getIncome() {
        return income;
    }

    public void setIncome(Date income) {
        this.income = income;
    }

    @Column(name="outcome")
    public Date getOutcome() {
        return outcome;
    }

    public void setOutcome(Date outcome) {
        this.outcome = outcome;
    }

    @Column(name="diagnosis")
    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Column(name="anamnesis")
    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    @Column(name="recommendations")
    public String getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(String recommendations) {
        this.recommendations = recommendations;
    }

    @Column(name="datedocument")
    public Date getDateDocument() {
        return dateDocument;
    }

    public void setDateDocument(Date dateDocument) {
        this.dateDocument = dateDocument;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="therapist")
    public TherapistEntity getTherapist() {
        return therapist;
    }

    public void setTherapist(TherapistEntity therapist) {
        this.therapist = therapist;
    }
}
