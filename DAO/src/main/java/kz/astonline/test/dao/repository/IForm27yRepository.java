package kz.astonline.test.dao.repository;

import kz.astonline.test.dao.repository.entities.Form27yEntity;
import kz.astonline.test.dao.repository.entities.TherapistEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by mad on 18.12.2016.
 */
@Repository
public interface IForm27yRepository extends JpaRepository<Form27yEntity, Long>, JpaSpecificationExecutor<Form27yEntity> {
    public List<Form27yEntity> findByTherapist(@Param("therapist")TherapistEntity therapist);
}
