package kz.astonline.test.dao.repository;

import kz.astonline.test.dao.repository.entities.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by mad on 18.12.2016.
 */
@Repository
public interface IPersonRepository extends JpaRepository<PersonEntity, Long>, JpaSpecificationExecutor<PersonEntity> {

}
