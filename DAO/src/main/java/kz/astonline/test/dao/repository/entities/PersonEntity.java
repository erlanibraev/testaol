package kz.astonline.test.dao.repository.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

/**
 * Created by mad on 18.12.2016.
 */
@Entity
@Table(name="person")
public class PersonEntity {
    private Long id;
    private String fio;
    private Date birthday;
    private String address;
    private String job;
    private Set<Form27yEntity> form27yEntities;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="fio")
    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    @Column(name="birthday")
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Column(name="address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name="job")
    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person", cascade = CascadeType.ALL)
    @JsonIgnore
    public Set<Form27yEntity> getForm27yEntities() {
        return form27yEntities;
    }

    public void setForm27yEntities(Set<Form27yEntity> form27yEntities) {
        this.form27yEntities = form27yEntities;
    }
}
