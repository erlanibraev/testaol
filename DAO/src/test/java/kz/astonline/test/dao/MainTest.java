package kz.astonline.test.dao;

import kz.astonline.test.dao.repository.IForm27yRepository;
import kz.astonline.test.dao.repository.IPersonRepository;
import kz.astonline.test.dao.repository.ITherapistRepository;
import kz.astonline.test.dao.repository.entities.Form27yEntity;
import kz.astonline.test.dao.repository.entities.PersonEntity;
import kz.astonline.test.dao.repository.entities.TherapistEntity;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mad on 18.12.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MainTest {

    @Autowired
    private IPersonRepository personRepository;

    @Autowired
    private ITherapistRepository therapistRepository;

    @Autowired
    private IForm27yRepository form27yRepository;

    @Test
    public void empty() {

    }

    @Test
    @Transactional
    public void test01() {
        PersonEntity person = new PersonEntity();
        person.setFio("Иванов Иван Иванович");
        person.setAddress("ул. Улица, дом 13, кв 314");
        person.setJob("РАБОТНИК на работе");
        person.setBirthday(new Date(new java.util.Date().getTime()));
        PersonEntity result = personRepository.save(person);
        printPerson(result);
    }

    @Test
    @Transactional
    public void test02() {
        TherapistEntity therapist = new TherapistEntity();
        therapist.setFio("Сидоров Сидор Сидорович");
        therapist.setUserName("test");
        therapist.setPassword("test");
        TherapistEntity result = therapistRepository.save(therapist);
        printTherapist(result);
    }

    @Test
    @Transactional
    public void test03() {
        PersonEntity person = personRepository.findOne(1L);
        TherapistEntity therapist = therapistRepository.findOne(1l);
        Form27yEntity form27y = new Form27yEntity();
        form27y.setPerson(person);
        form27y.setTherapist(therapist);
        form27y.setAnamnesis("asasdasd");
        form27y.setDiagnosis("asdasdasd");
        form27y.setDirectionToHospital("asasdasd");
        form27y.setRecommendations("sdasdasd");
        Form27yEntity result = form27yRepository.save(form27y);
        printForm27y(result);
    }

    @Test
    public void test04() {
        List<TherapistEntity> list = therapistRepository.findAll();
        list
                .forEach(therapist -> printTherapist(therapist) );
    }

    @Test
    public void test05() {
        personRepository
                .findAll()
                .forEach(person -> printPerson(person));
    }

    @Test
    public void test06() {
        TherapistEntity therapist = therapistRepository.findOneByUserName("test1");
        printTherapist(therapist);
        form27yRepository
                .findByTherapist(therapist)
                .forEach(form27y -> printForm27y(form27y));
    }

    private void printForm27y(Form27yEntity form27y) {
        System.out.print(form27y.getId());
        System.out.print(") ");
        System.out.print(form27y.getPerson().getFio());
        System.out.print("; ");
        System.out.print(form27y.getDateDocument());
        System.out.print("; ");
        System.out.print(form27y.getTherapist().getFio());
        System.out.print("; ");
        System.out.println();
    }

    private void printTherapist(TherapistEntity therapist) {
        System.out.print(therapist.getId());
        System.out.print(") ");
        System.out.print(therapist.getFio());
        System.out.print("; ");
        System.out.print(therapist.getUserName());
        System.out.println("; ");
    }

    private void printPerson(PersonEntity person) {
        System.out.print(person.getId());
        System.out.print(") ");
        System.out.print(person.getFio());
        System.out.print("; ");
        System.out.print(person.getBirthday());
        System.out.print("; ");
        System.out.print(person.getAddress());
        System.out.print("; ");
        System.out.print(person.getJob());
        System.out.println("; ");
    }
}